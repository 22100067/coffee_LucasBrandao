//
//  ViewController.swift
//  coffee_LucasBrandao
//
//  Created by COTEMIG on 10/11/22.
//

import UIKit
import Alamofire
import Kingfisher

struct Cafes: Decodable {
    let image: String
}

class ViewController: UIViewController, UITableViewDataSource {
    
    var Lista: [Cafes] = []
    var userDefaults = UserDefaults.standard
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Lista.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = TableView.dequeueReusableCell(withIdentifier: "MeuCafe", for: indexPath) as! MeuCafe
        let coisa = Lista[indexPath.row]
        item.Imagem.kf.setImage(with: URL(string: coisa.image))
        return item
    }
    
    func Cafe(){
        AF.request("https://coffee.alexflipnote.dev/random.json").responseDecodable(of: [Cafes].self) { response in
            if let cafe = response.value {
                self.Lista = cafe
                print(cafe)
            }
            self.TableView.reloadData()
}
    }

    @IBOutlet weak var TableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        TableView.dataSource = self
        Cafe()
}
}
